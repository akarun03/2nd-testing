package com.framework.pages;

import org.openqa.selenium.WebElement;

import com.framework.design.ProjectMethods;

public class CreateLead extends ProjectMethods {

	public CreateLead() {

	}

	public CreateLead EnterCompanyName() {

		WebElement CompanyName = locateElement("id", "createLeadForm_companyName");
		clearAndType(CompanyName, "TCS");
		return this;
	}

	public CreateLead EnterFirstName() {
		WebElement FirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(FirstName, "Arun");
		return this;
	}

	public CreateLead EnterLastName() {

		WebElement LastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(LastName, "ak");		
		return this;
	}

	public ViewLead ClicktheCreateLead() {

		WebElement CreateLead = locateElement("class", "smallSubmit");
		click(CreateLead);
		return new ViewLead();
	}
}
