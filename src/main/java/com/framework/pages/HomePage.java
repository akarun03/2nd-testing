package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogout;
	public LoginPage clickLogout() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);
		return new LoginPage();
		
	}
	
	@FindBy(how=How.XPATH,using="//a[contains(text(),'CRM/SFA')]") WebElement ClickCRM;
	public MyHomePage ClickCRMLink() {
		//*WebElement ClickCRM = locateElement("xpath", "//a[contains(text(),'CRM/SFA')]");
		click(ClickCRM);
		return new MyHomePage();
	}
	
	
	/*@FindBy(how=How.XPATH,using="//a[contains(text(),'Create Lead')]") WebElement ClickCreateLead;
	public CreateLead ClickCreadLead() {
		
		//WebElement CLickCreateLead = locateElement("xpath", "//a[contains(text(),'Create Lead')]");
		click(ClickCreateLead);
		return new CreateLead();

	}
*/
	
	
	
	
}
















