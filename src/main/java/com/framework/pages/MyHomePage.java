package com.framework.pages;

import org.openqa.selenium.WebElement;

import com.framework.design.ProjectMethods;

public class MyHomePage extends ProjectMethods {
	

	public MyHomePage ClickLead() {
		
		WebElement ClickLead = locateElement("xpath", "//a[contains(text(),'Leads')]");
		click(ClickLead);
		return this;
	}

	
	public CreateLead ClickCreateLead() {
		WebElement ClickCreateLead = locateElement("xpath", "//a[contains(text(),'Create Lead')]");
		click(ClickCreateLead);
		return new CreateLead();
	}
}
