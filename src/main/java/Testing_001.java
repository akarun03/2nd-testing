import java.util.Locale.Category;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class Testing_001 extends ProjectMethods{
	
	@BeforeTest
	public void setData()
	{
	testCaseName="TC001_loginpage";
	testDescription ="Login into Leaftaps";
	testNodes="Leeds";
	author="Arun";
	category="Smoke";
	
	}
	

	@Test
	public void createlead()
	{
		new LoginPage()
		.enterUsername()
		.enterPassword()
		.clickLogin()
		.ClickCRMLink()
		.ClickLead()
		.ClickCreateLead()
		.EnterCompanyName()
		.EnterFirstName()
		.EnterLastName()
		.ClicktheCreateLead();
	}
	
}
